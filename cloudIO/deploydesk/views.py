from django.shortcuts import render

# Create your views here.
from django.http import HttpResponseRedirect, response
from django.shortcuts import render
from .forms import UploadFileForm
from django.views.generic.edit import FormView
from .forms import FileFieldForm
import zipfile
from cloudIO import settings
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

# Imaginary function to handle an uploaded file.
from .forms import handle_uploaded_file

# def upload_file(request):
#     if request.method == 'POST':
#         form = UploadFileForm(request.POST, request.FILES)
#         if form.is_valid():
#             handle_uploaded_file(request.FILES['file'])
#             return HttpResponseRedirect('/success/url/')
#     else:
#         form = UploadFileForm()
#     return render(request, 'upload.html', {'form': form})


# class FileFieldFormView(FormView):
#     form_class = FileFieldForm
#     template_name = '/upload.htmls'  # Replace with your template.
#     success_url = '...'  # Replace with your URL or reverse().

#     def post(self, request, *args, **kwargs):
#         form_class = self.get_form_class()
#         form = self.get_form(form_class)
#         files = request.FILES.getlist('file_field')
#         if form.is_valid():
#             for f in files:
#                 ...  # Do something with each file.
#             return self.form_valid(form)
#         else:
#             return self.form_invalid(form)
@api_view(('POST',))
def upload_file(request):
    if request.method == 'POST':
        # form = ModelFormWithFileField(request.POST, request.FILES)
        # if form.is_valid():
        #     # file is saved
        #     form.save()
        #     return HttpResponseRedirect('/success/url/')
        uploadFile = request.data['file']
        target = settings.MEDIA_ROOT
        with zipfile.ZipFile(uploadFile,"r") as zip_ref:
            zip_ref.extractall(target)


    # else:
    #     form = ModelFormWithFileField()
    return Response(status=status.HTTP_201_CREATED)
