from django.apps import AppConfig


class DeploydeskConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'deploydesk'
