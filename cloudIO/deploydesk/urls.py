from django.urls import path, include
from .views import upload_file
urlpatterns = [
    # path('ordercount/<pk>/',ordercount,name="ordercount"),
    path('files/upload', upload_file , name="uploadFile"),
]